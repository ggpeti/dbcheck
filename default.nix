# nix-shell environment for provisioning infrastructure and deployment
with import ./pin.nix {};
mkShell rec {
  buildInputs = [ mssqlcert ];

  mssqlcert = writeShellScriptBin "mssqlcert" ''
    # Get the certificate of a Microsoft SQL Server
    ${python3}/bin/python ${./mssqlcert.py} $@
  '';
}
